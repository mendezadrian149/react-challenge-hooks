import React from "react";
import '../../src/';
import logo from '../img/linux.png';
import { Link } from 'react-router-dom';


const Header = () => {
    return (
        <div className="Navbar">
        <div className="container-fluid">
            <Link to='/' className="Navbar__brand">
               <img className="Navbar__brand-logo" alt="Logo" src={logo} />
                <span className="font-weight-light">Github Api</span>
                <span className="font-weight-bold">Challenge</span>
            </Link>
        </div>
      </div>
    );
};

export default Header;