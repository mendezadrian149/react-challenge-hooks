import React, { useState } from "react";
import axios from "axios";
import Repository from "./Repository";
import Loading from "./Loading";


const Search = () => {
  const [search, setSearch] = useState('');
  const [data, setData] = useState('');
  const [isSubmit, setSubmit] = useState(false);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {

      const response = await fetchData();

      setSubmit(true);

      if (response.items && response.items.length) {
        setData(response.items);
        setPage(prevCount => prevCount + 1);
      } else {
        setData('');
      }


    } catch (error) {
      console.log(error);
    }

  }


  const loadMore = async (e) => {
    e.preventDefault();

    try {
      setPage(prevCount => prevCount + 1);
      const moreItems = await fetchData();
      if (moreItems.items && moreItems.items.length) {
        setData(data.concat(moreItems.items))
        setSubmit(true);
      } else {
        setData('');
      }

    } catch (err) {
      console.log(err);
      return false;
    }
  }

  const fetchData = async () => {
    try {
      setLoading(true);
      const response = await axios.get(`https://api.github.com/search/repositories?q=${search}&page=${page}&per_page=6`);
      setLoading(false);
      return response.data;
    } catch (err) {
      return err;
    }

  }

  let notFound;

  if (data === '' && isSubmit) {
    notFound = <h1>Not Found</h1>
  }

  return (

    <div>
      <form onSubmit={handleSubmit}>
        <div className="input-group mb-3">
          <input className="form_field"
            type="text"
            name="search"
            value={search}
            onChange={e => setSearch(e.target.value)}
            placeholder="Buscar repositorios"
            aria-describedby="basic-addon2"
          />
          <button type="submit" className="btn btn-info">Buscar</button>
        </div>
      </form>
      <div>
        <Loading load={loading} />
        {isSubmit && data ? <Repository data={data} /> : ''}
        {notFound}

      </div>
      {data ? <button className="btn btn-info" type="button" onClick={loadMore} > Load More ... </button> : ''}
    </div>
  );
}

export default Search;