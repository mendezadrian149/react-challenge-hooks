import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './Navbar.css';


ReactDOM.render(<App />, document.getElementById('root'));

