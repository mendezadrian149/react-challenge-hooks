import React from 'react';
import loading from '../img/loading.gif';


const Loading = (props) => {
  return (
    <div>
      {props.load ? (
        <img src={loading} className="" />
      ) : ''}
    </div>
  )
}

export default Loading;