import React from 'react';
import { Link } from 'react-router-dom';

const Repository = (props) => {
  const card = props.data.map((info, key) =>
    <div key={key++} className="card box">
      <img src={info.owner.avatar_url} alt="Avatar" />
      <div className="container">
        <h4><b>{info.full_name}</b></h4>
        <p>{info.description}</p>
        <p>
          <Link to={{ pathname: '/Contributor/', state: { repos: info.contributors_url}}} className="card-text text-success"> Contributors </Link>
        </p>
      </div>
    </div>
  );

  return (
    <div>
      {card}
    </div>
  );
};

export default Repository;