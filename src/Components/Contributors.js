import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import Loading from "./Loading";

const Contributor = (props) => {
  const [state, setState] = useState('');
  const [isLoad, setLoad] = useState(false);
  const [page, setPage] = useState(1);

  useEffect(() => {
    console.log('ejecutando efecto');
    fetchData().then((result) => {
      if (result.data !== '') {
        setPage(prevCount => prevCount + 1);
        setState(result.data);
        setLoad(true);
      }

    }).catch((err) => {
      return err;
    });;

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const fetchData = async () => {
    try {
      const response = await Axios.get(props.location.state.repos + `?page=${page}&per_page=5`);
      return response;
    } catch (err) {
      console.log(err);
    }
  }
  const handleClick = async (e) => {

    e.preventDefault();
    // setLoad(false);
    
    const resp = await fetchData();
    setPage(prevCount => prevCount + 1);
    setState(state.concat(resp.data))
    setLoad(true);

  }
  let result;
  {
    isLoad ?
      result = state.map((info, key) =>
        <div key={key++} className="card box">
          <img src={info.avatar_url} alt="Avatar" />
          <div className="container">
            <h4><b>{info.login}</b></h4>
            <p>{info.description}</p>
          </div>
        </div>
      ) : (
        result = <Loading load={true} />
      )
  }
  return (
    <div className="">
      {result}

      <div className="">
        {state && state.length ? <button type="button" onClick={handleClick} className="btn">Load More ...</button> : <h1>No Contributors</h1>}
      </div>
    </div>
  )

}


export default Contributor;