import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import './App.css';
import Header from './Components/Header';
import Search from './Components/Search';
import Contributor from './Components/Contributors';

function App() {
  console.log(Search);
  return (
    <div className="App">
      <BrowserRouter>
        <Header />

        <Route
          path='/'
          exact
          component={Search}
        />

        <Route
          path='/Contributor'
          exact
          component={Contributor}
        />

      </BrowserRouter>
    </div>
  );
}

export default App;
